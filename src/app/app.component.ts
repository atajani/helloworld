import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
      <h1> Hello World !</h1>
      <div>Apps is generated with ng, built with gitlab cli and deployed with netlify</div>
  `,
  styles: []
})
export class AppComponent {
  title = 'app';
}
